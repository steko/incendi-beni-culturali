---
title: "Aree percorse dal fuoco in Liguria - Beni culturali e paesaggistici a rischio"
author: "Stefano Costa"
date: "14 agosto 2019"
output: html_document
bibliography: incendi.bib
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
library(sf)
library(ggplot2)
library(lwgeom)
library(purrr)
library(plyr)
```

## Gli incendi e il patrimonio culturale

[@Taboroff_CulturalheritageNatural_2000]

## L'impatto sul patrimonio archeologico in Italia

Sono numerosi i siti archeologici italiani interessati da incendi in anni recenti. Solo a titolo di esempio:

- Faragola (2017)
- Elea/Velia (2017)
- Himera (2017)
- Centocelle (2017)
- Selinunte (2016)
- San Vincenzo al Volturno (2015)
- Rosarno (2012)

## L'impatto sul patrimonio archeologico in Liguria

### Procedura


Calcolare:

- quanti beni si trovano in apf (e quante volte)
- quanti beni si trovano in buffer di apf (o viceversa calcolare un buffer delle aree archeologiche, anche disponibili in forma puntuale)



### Fonti di dati

- aree sottoposte a vincolo archeologico
- vincoli archeologici puntuali (buffer di 10 m)
- siti archeologici presenti nel SIGEC (puntuali o areali)
- zone di interesse archeologico (art. 142 Codice BBCC)

Così importiamo tutti i vincoli archeologici areali:

```{r vincoliimport}
paths_vincoli <- dir("LiguriaVincoli_2019-08-16", pattern = "*\\.zip$", full.names = TRUE)
map(paths_vincoli, unzip, exdir="LiguriaVincoli_2019-08-16")
apf_shp_paths_vincoli <- sub("\\.zip", ".shp", paths_vincoli)
names(apf_shp_paths_vincoli) <- basename(apf_shp_paths_vincoli)
res <- map(apf_shp_paths_vincoli, st_read)
archeo_wgs84 <- st_transform(res$Vincoli_Archeologici.shp, 3003)
archeo_wgs84_puntuali <- st_transform(res$Vincoli_Archeologici_Puntuali.shp, 3003)
```

Calcoliamo un buffer di circa 10 m per i vincoli puntuali:

```{r archeopuntualibuffer}
archeo_wgs84_puntuali_buffer <- st_buffer(archeo_wgs84_puntuali, 10 )
plot(st_geometry(archeo_wgs84_puntuali_buffer), col="grey")
```

che poi vengono accorpati a quelli areali:

```{r}
archeo_totale <- rbind(subset(archeo_wgs84 , select = -DATA_AGG ), archeo_wgs84_puntuali_buffer)
```


Importiamo le aree percorse dal fuoco:

```{r apfimport}
inc_1996_2016_tidy <- st_read("incendi-liguria-shape-1996-2016.shp")
inc_1996_2016_tidy <- st_transform(inc_1996_2016_tidy, 3003)
```

E infine calcoliamo l'intersezione tra le due categorie TODO:

```{r}
# i due data frame separati si intersecano senza ptroblemi
archeo_int_a <- st_intersection(archeo_wgs84, inc_1996_2016_tidy)
archeo_int_p <- st_intersection(archeo_wgs84_puntuali_buffer, inc_1996_2016_tidy)
archeo_totale <- rbind(subset(archeo_int_a , select = -DATA_AGG ), archeo_int_p)
```

Quali e quante sono queste aree di intersezione?

```{r}
archeo_int_table <- archeo_totale[,c("TIPO_VINCO", "COMUNE", "LOCALITA", "DATA_INC")]
archeo_int_table$geometry <- NULL
```

Sono `r length(archeo_int_table$DATA_INC)` corrispondenze totali.

```{r}
knitr::kable(archeo_int_table)
```

Mentre questa carta mostra la sovrapposizione tra aree vincolate archeologiche e superfici percorse da incendio.

```{r importlimiti}
province <- st_read("LimitiAmministrativi/Province.shp")
province <- st_transform(province, 3003)
comuni <- st_read("LimitiAmministrativi/Comuni.shp")
comuni <- st_transform(comuni, 3003)
```


```{r mappa}
ggplot(data=province) +
  theme_bw() +
  geom_sf(fill=NA) +
  geom_sf(data=inc_1996_2016_tidy, fill="grey", colour=NA) +
  geom_sf(data=archeo_totale, fill="red", colour=NA)
```

Zoom sulla provincia di Imperia:


```{r mappaimperia}
ggplot(data=comuni) +
  theme_bw() +
  geom_sf(fill=NA, colour="grey") +
  geom_sf(data=province, fill=NA) +
  geom_sf(data=inc_1996_2016_tidy, fill="black", colour=NA, alpha=0.7) +
  geom_sf(data=archeo_totale, fill="red", colour=NA) +
  #geom_text(data=archeo_totale)
  coord_sf(xlim=c(7.45, 8.2), ylim=c(43.75, 44.15), crs=4326)
```

## Osservazioni

A livello generale, data la natura dei beni archeologici immobili, si può affermare che anche prima del formale riconoscimento come sito archeologico ci sia la presenza di beni che quindi possono aver subito danni a seguito dell'incendio. In altre parole se c'è un incendio nel 2000 e l'area viene vincolata nel 2005 non possiamo affermare che non ci sia stato un danno al sito archeologico, che evidentemente era già presente.

## Riferimenti bibliografici