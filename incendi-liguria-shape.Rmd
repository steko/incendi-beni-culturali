---
title: "Aree percorse dal fuoco in Liguria - 1996-2016"
author: "Stefano Costa"
date: "14 agosto 2019"
output: html_document
bibliography: incendi.bib
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
library(sf)
library(lubridate)
library(lwgeom)
library(purrr)
library(ggplot2)
```


## Procedura

- Scaricare i dati sulle aree percorse dal fuoco (apf)
- Normalizzarli sotto forma di unico dataset in cui ogni poligono ha l'attributo dell'anno.


## Analisi dell'impatto degli incendi in Liguria

Importiamo i dati per il periodo 1996-2002.

```{r dataimport_1996_2002}
paths_1996_2002 <- dir("Incendi_1996-2002", pattern = "*Incendi.*\\.zip$", full.names = TRUE)
map(paths_1996_2002, unzip, exdir="Incendi_1996-2002")
apf_shp_paths_1996_2002 <- sub("\\.zip", ".shp", paths_1996_2002)
names(apf_shp_paths_1996_2002) <- basename(apf_shp_paths_1996_2002)
res <- map(apf_shp_paths_1996_2002, st_read)

tidy_1996_2002 <- function(shp) {
  shp_tidy <- subset(shp, select=c("MSLINK", "ID_TIPO", "DATA_EVENT", "FN_AIB", "geometry"))
  shp_tidy <- subset(shp, select=c("MSLINK", "ID_TIPO", "DATA_EVENT", "FN_AIB", "geometry"))
  shp_tidy$DATA_INC <- shp_tidy$DATA_EVENT
  shp_tidy$DATA_EVENT <- NULL
  shp_tidy$ID <- shp_tidy$MSLINK
  shp_tidy$MSLINK <- NULL
  shp_tidy <- st_transform(shp_tidy, 3003)
  shp_tidy
}

res_tidy <- map(res, tidy_1996_2002)
inc_1996_2002_tidy <- do.call(rbind, res_tidy)
```

Importiamo i dati per il periodo 2003-2016.

```{r dataimport_2003_2016}
paths_2003_2016 <- dir("Incendi_2003-2016", pattern = "*Incendi.*\\.zip$", full.names = TRUE)
map(paths_2003_2016, unzip, exdir="Incendi_2003-2016")
apf_shp_paths_2003_2016 <- sub("\\.zip", ".shp", paths_2003_2016)
names(apf_shp_paths_2003_2016) <- basename(apf_shp_paths_2003_2016)
res <- map(apf_shp_paths_2003_2016, st_read)

tidy_2003_2016 <- function(shp) {
  shp_tidy <- subset(shp, select=c("ID", "ID_TIPO", "DATA_INC", "FN_AIB", "geometry"))
  shp_tidy <- st_transform(shp_tidy, 3003)
  shp_tidy
}

res_tidy <- map(res, tidy_2003_2016)
inc_2003_2016_tidy <- do.call(rbind, res_tidy)
```

Uniamo tutti i dati in un singolo data frame.

```{r datamerge_1996_2016}
inc_1996_2016 <- list(inc_1996_2002_tidy, inc_2003_2016_tidy)
inc_1996_2016_tidy <- do.call(rbind, inc_1996_2016)
inc_1996_2016_tidy <- st_transform(inc_1996_2016_tidy, 3003)
inc_1996_2016_tidy$DATA_INC <- as.Date(inc_1996_2016_tidy$DATA_INC, format="%Y%m%d")
```

Poiché alcune geometrie risultano non valide topologicamente, effettuiamo anche una operazione di validazione:

```{r}
inc_1996_2016_tidy <- lwgeom::st_make_valid(inc_1996_2016_tidy)
```


Questa carta mostra l'intera superficie percorsa da incendio in Liguria nel periodo 1996-2016.

```{r importlimiti}
regione <- st_read("LimitiAmministrativi/Regione.shp")
regione <- st_transform(regione, 3003)
```


```{r mappa}
ggplot(data=regione) +
  theme_bw() +
  geom_sf(fill=NA) +
  geom_sf(data=inc_1996_2016_tidy, fill="black", colour=NA, alpha=0.6)
```

Il risultato viene esportato in formato Shapefile per successive elaborazioni.

```{r dataexport}
st_write(inc_1996_2016_tidy, "incendi-liguria-shape-1996-2016.shp")
```

